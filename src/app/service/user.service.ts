import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../api/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
    private apiUrl = environment.baseUrl + '/Usuario'; // Use a variável de ambiente

    constructor(private http: HttpClient) {}

    getUsers() {
      return this.http.get<User[]>(this.apiUrl)
      .toPromise()
        .then(res => res as User[])
        .then(data => data);
    }

    addUser(user: User): Observable<User> {
        return this.http.post<User>(this.apiUrl, user);
    }

    updateUser(user: User): Observable<User> {
        return this.http.put<User>(`${this.apiUrl}/${user.codigo}`, user);
    }

    deleteUser(codigo: number) {
        return this.http.delete(`${this.apiUrl}/${codigo}`);
    }}
