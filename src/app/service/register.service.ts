// register.service.ts

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment'; // Importe o environment

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private apiUrl = environment.baseUrl + '/Auth/signup'; // Use a variável de ambiente

  constructor(private http: HttpClient) {}

  register(formData: any): Observable<any> {
    return this.http.post<any>(this.apiUrl, formData);
  }
}
