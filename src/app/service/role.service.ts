import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Role } from '../api/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
    private apiUrl = environment.baseUrl + '/Perfil'; // Use a variável de ambiente

    constructor(private http: HttpClient) {}

    getRoles() {
      return this.http.get<Role[]>(this.apiUrl)
      .toPromise()
        .then(res => res as Role[])
        .then(data => data);
    }

    addRole(role: Role): Observable<Role> {
        return this.http.post<Role>(this.apiUrl, role);
    }

    updateRole(role: Role): Observable<Role> {
        return this.http.put<Role>(`${this.apiUrl}/${role.codigo}`, role);
    }

    deleteRole(role: any) {
        return this.http.delete(`${this.apiUrl}/${role.codigo}`);
    }
}
