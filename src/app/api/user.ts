import { Role } from "./role";

export interface User {
    nome?: string;
    codigo?: number;
    created?: string;
    modified?: string;
    perfil?: Role;
}
