import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleDeleteConfirmComponent } from './role-delete-confirm.component';

describe('RoleDeleteConfirmComponent', () => {
  let component: RoleDeleteConfirmComponent;
  let fixture: ComponentFixture<RoleDeleteConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RoleDeleteConfirmComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RoleDeleteConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
