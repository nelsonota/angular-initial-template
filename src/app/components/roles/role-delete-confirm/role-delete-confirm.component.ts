import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { RoleService } from 'src/app/service/role.service';

@Component({
  selector: 'app-role-delete-confirm',
  standalone: true,
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule,
  ],
  templateUrl: './role-delete-confirm.component.html',
  styleUrl: './role-delete-confirm.component.scss'
})
export class RoleDeleteConfirmComponent {
    @Input() display: boolean;
    @Input() set role(value: any) {
      this._role = value;
    }
    @Output() onHide: EventEmitter<any> = new EventEmitter();
    protected _role: any;

    constructor(private roleService: RoleService) {}

    confirmDelete() {
        this.roleService.deleteRole(this._role).subscribe({
            next: () => {
                console.log('Role deleted');
                this.onHide.emit();
            },
            error: (err) => console.error('Error deleting role', err)
        });
    }
}
