import { CommonModule } from '@angular/common';
import { Component, Input, Output, EventEmitter, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RoleService } from 'src/app/service/role.service';

@Component({
    selector: 'app-role-form',
    standalone: true,
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DialogModule,
        ButtonModule,
        InputTextModule,
        InputTextareaModule
    ],
    templateUrl: './role-form.component.html',
    styleUrl: './role-form.component.scss'
})
export class RoleFormComponent {
  @Input() display: boolean; // Controla a visibilidade do diálogo
  @Input() set role(value: any) {
    this._role = value;
    this.roleForm.patchValue({'nome': value.nome});
  }
  @Output() onHide: EventEmitter<any> = new EventEmitter();
  private _role: any;
  roleForm: FormGroup;

  constructor(private roleService: RoleService) {
    this.roleForm = new FormGroup({
      nome: new FormControl(null, Validators.required)
    });
  }

  saveRole() {
    if (this.roleForm.valid) {
      this._role.nome = this.roleForm.get('nome').value;
      if (this._role.codigo) {
        this.roleService.updateRole(this._role).subscribe({
          next: (updatedRole) => {
            console.log('Role updated', updatedRole);
            this.onHide.emit();
          },
          error: (err) => console.error('Error updating role', err)
        });
        } else {
        this.roleService.addRole(this._role).subscribe({
          next: (newRole) => {
            console.log('Role added', newRole);
            this.onHide.emit();
          },
          error: (err) => console.error('Error adding role', err)
        });
      }
    }
  }
}
