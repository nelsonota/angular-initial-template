import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';
import { Role } from 'src/app/api/role';
import { RoleService } from 'src/app/service/role.service';
import { RoleFormComponent } from "./role-form/role-form.component";
import { CommonModule } from '@angular/common';
import { RoleDeleteConfirmComponent } from './role-delete-confirm/role-delete-confirm.component';

@Component({
    selector: 'app-roles',
    standalone: true,
    providers: [
        MessageService
    ],
    templateUrl: './roles.component.html',
    styleUrl: './roles.component.scss',
    imports: [
        CommonModule,
        TableModule,
        ToastModule,
        ToolbarModule,
        DialogModule,
        ButtonModule,
        InputTextModule,
        RoleFormComponent
    ]
})
export class RolesComponent implements OnInit {
    @ViewChild('roleFormContainer', { read: ViewContainerRef }) roleFormContainer: ViewContainerRef;
    cols: any[] = [];
    roles: any[] = [];
    selectedRoles: Role[] = [];
    selectedRole: any;
    roleDialog: boolean = false;
    deleteRoleDialog: boolean = false;
    deleteRolesDialog: boolean = false;
    loading: boolean = true;

    constructor(private messageService: MessageService, private roleService: RoleService) { }

    ngOnInit(): void {
      this.loadRoles();
      this.cols = [
        { field: 'nome', header: 'Nome' },
        { field: 'codigo', header: 'Codigo' },
        { field: 'created', header: 'created' },
        { field: 'modified', header: 'modified' }
      ];
    }

    // onDialogHide() {
    //     this.roleDialog = false;
    // }

    // onRoleSaved(saved: boolean) {
    //     if (saved) {
    //       this.loadRoles(); // Atualizar a lista de roles
    //     }
    // }

    editRole(role?: any) {
      this.roleFormContainer.clear();
      const componentRef = this.roleFormContainer.createComponent<RoleFormComponent>(RoleFormComponent);
      componentRef.instance.role = role || {}; // Passa um novo ou o role existente
      componentRef.instance.display = true;
      componentRef.instance.onHide.subscribe(() => {
        this.roleFormContainer.clear();
        this.loadRoles();
      });
    }

    deleteRole(role?: any) {
        this.roleFormContainer.clear();
        const componentRef = this.roleFormContainer.createComponent<RoleDeleteConfirmComponent>(RoleDeleteConfirmComponent);
        componentRef.instance.role = role || {}; // Passa um novo ou o role existente
        componentRef.instance.display = true;
        componentRef.instance.onHide.subscribe(() => {
          this.roleFormContainer.clear();
          this.loadRoles();
        });
    }

    loadRoles() {
        this.loading = true;
        this.roleService.getRoles().then(data => {
            this.roles = data;
            this.loading = false;
        });
    }
}
