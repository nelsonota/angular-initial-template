import { CommonModule } from '@angular/common';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';
import { MessageService } from 'primeng/api';
import { UserService } from 'src/app/service/user.service';
import { UserFormComponent } from './user-form/user-form.component';
import { User } from 'src/app/api/user';

@Component({
  selector: 'app-users',
  standalone: true,
  providers: [
    MessageService
  ],
  imports: [
    CommonModule,
    TableModule,
    ToastModule,
    ToolbarModule,
    DialogModule,
    ButtonModule,
    InputTextModule
  ],
  templateUrl: './users.component.html',
  styleUrl: './users.component.scss'
})
export class UsersComponent implements OnInit {
    @ViewChild('userFormContainer', { read: ViewContainerRef }) userFormContainer: ViewContainerRef;
    loading: boolean = true;
    users: any[] = [];
    cols: any[] = [];

    constructor(private messageService: MessageService, private userService: UserService) { }

    ngOnInit(): void {
        this.loadUsers();
        this.cols = [
          { field: 'nome', header: 'Nome' },
          { field: 'codigo', header: 'Codigo' },
          { field: 'created', header: 'created' },
          { field: 'modified', header: 'modified' }
        ];
      }

    loadUsers() {
        this.loading = true;
        this.userService.getUsers().then(data => {
            this.users = data;
            this.loading = false;
        });
    }

    editUser(user?: any) {
        this.userFormContainer.clear();
        const componentRef = this.userFormContainer.createComponent<UserFormComponent>(UserFormComponent);
        componentRef.instance.user = user || {}; // Passa um novo ou o user existente
        componentRef.instance.display = true;
        componentRef.instance.onHide.subscribe(() => {
          this.userFormContainer.clear();
          this.loadUsers();
        });
    }

    deleteUser(user: User): void {
        if (confirm(`Are you sure you want to delete user ${user.nome}?`)) {
          this.userService.deleteUser(user.codigo).subscribe(() => {
            this.loadUsers();
          }, (error) => {
            console.error('Error deleting user:', error);
          });
        }
      }
}
