import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { Role } from 'src/app/api/role';
import { User } from 'src/app/api/user';
import { RoleService } from 'src/app/service/role.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule
  ],
  templateUrl: './user-form.component.html',
  styleUrl: './user-form.component.scss'
})
export class UserFormComponent implements OnInit {
    @Input() display: boolean; // Controla a visibilidade do diálogo
    @Input() set user(value: any) {
      this._user = value;
      this.userForm.patchValue({'nome': value.nome});
      this.userForm.patchValue({'email': value.email});
      this.userForm.patchValue({'perfil': value.perfil});
    }
    @Output() onHide: EventEmitter<any> = new EventEmitter();
    protected _user: any;
    userForm: FormGroup;
    roles: Role[] = [];

    constructor(private userService: UserService, private roleService: RoleService) {
        this.userForm = new FormGroup({
          nome: new FormControl(null, Validators.required),
          email: new FormControl(null, Validators.required),
          perfil: new FormControl(null, Validators.required)
        });
    }

    ngOnInit(): void {
        this.loadRoles();
    }

    loadRoles(): void {
        this.roleService.getRoles().then((roles) => {
          this.roles = roles;
        });
    }

    saveUser() {
        if (this.userForm.valid) {
            this._user.nome = this.userForm.get('nome').value;
            this._user.perfil = this.userForm.get('perfil').value;
            if (this._user.codigo) {
                this.userService.updateUser(this._user).subscribe(
                    (user) => {
                        this.onHide.emit();
                    },
                    (error) => {
                        console.error('Error updating user:', error);
                    }
                );
            } else {
                this._user.email = this.userForm.get('email').value;
                this.userService.addUser(this._user).subscribe({
                    next: (newUser) => {
                      console.log('User added', newUser);
                      this.onHide.emit();
                    },
                    error: (err) => console.error('Error adding role', err)
                });
            }
          }
      }
}
