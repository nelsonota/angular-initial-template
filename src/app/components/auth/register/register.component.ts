import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxMaskDirective, provideNgxMask } from 'ngx-mask';
import { ToastrService } from 'ngx-toastr';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { RegisterService } from 'src/app/service/register.service';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [
    CommonModule,
    InputTextModule,
    ButtonModule,
    ReactiveFormsModule,
    NgxMaskDirective
  ],
  providers: [
    provideNgxMask(),
  ],
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent {
  registerForm: FormGroup;

  constructor(private fb: FormBuilder, public layoutService: LayoutService, private registerService: RegisterService, private toastr: ToastrService, private router: Router) {
    this.createForm();
  }

  createForm() {
    this.registerForm = this.fb.group({
      documento: ['', [Validators.required, this.validateCnpjCpf]],
      razaoSocial: ['', [Validators.required, this.validateRazaoSocial]],
      email: ['', [Validators.required, Validators.email]],
      usuario: ['', [Validators.required, Validators.minLength(3)]],
      senha: ['', [Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)]],
      confirmarSenha: ['', Validators.required]
    }, { validator: this.passwordMatchValidator });
  }

  validateCnpjCpf(control) {
    const cnpjCpf = control.value;
    // Lógica de validação de CNPJ/CPF
    return null; // Retornar null se for válido, ou objeto com erro caso contrário
  }

  validateRazaoSocial(control) {
    const razaoSocial = control.value;
    // Lógica de validação de Razão Social
    return null; // Retornar null se for válido, ou objeto com erro caso contrário
  }

  passwordMatchValidator(formGroup: FormGroup) {
    const password = formGroup.get('senha').value;
    const confirmPassword = formGroup.get('confirmarSenha').value;
    return password === confirmPassword ? null : { 'mismatch': true };
  }

  documentoMask(): string {
    const documentoValue = this.registerForm.get('documento').value;
    if (documentoValue && documentoValue.length > 11) {
      return '00.000.000/0000-00';
    } else {
      return '000.000.000-000';
    }
  }

  onSubmit() {
    if (this.registerForm.valid) {
        let formData = { ...this.registerForm.value }; // Copiar os dados do formulário
        // Remover a máscara do valor do campo de documento
        formData.documento = formData.documento.replace(/\D/g, ''); // Remove todos os caracteres não numéricos
        this.registerService.register(formData).subscribe(
            response => {
                this.toastr.success('Registro bem-sucedido');
                this.router.navigate(['/']);
            },
            error => {
                this.toastr.error('Erro ao registrar');
            }
          );
      } else {
        this.validateAllFormFields(this.registerForm);
      }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      } else {
        control.markAsTouched({ onlySelf: true });
      }
    });
  }
}
