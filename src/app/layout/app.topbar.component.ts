import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from "./service/app.layout.service";
import { KeycloakService } from 'keycloak-angular';
import { Router } from '@angular/router';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent implements OnInit {
    menuSettings: MenuItem[] = [];
    menuUser: MenuItem[] = [];
    items!: MenuItem[];

    @ViewChild('menubutton') menuButton!: ElementRef;

    @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

    @ViewChild('topbarmenu') menu!: ElementRef;

    constructor(public layoutService: LayoutService, private keycloakService: KeycloakService, private router: Router) { }
    ngOnInit(): void {
        this.menuSettings = [
            {
                label: 'Perfis', icon: 'pi pi-fw pi-clone', routerLink: ['/a/roles']
            },
            {
                label: 'Usuários', icon: 'pi pi-fw pi-users', routerLink: ['/a/users']
            },
        ];
        this.menuUser = [
            {
                label: 'Sair', icon: 'pi pi-fw pi-sign-out', command: () => {
                    this.keycloakService.logout();
                    // this.router.navigate(['/']);
                }
            },
        ];
    }
}
